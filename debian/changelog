jaaa (0.9.2-1) unstable; urgency=medium

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * Use dh-compat instead of debian/compat
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Olivier Humbert ]
  * d/*.desktop: Add French names and comments
  * d/copyright: Update year, http > https and add myself
  * http > https in d/control and d/watch

  [ Dennis Braun ]
  * New upstream release
  * d/control:
    + Bump dh-compat to 12
    + Bump Standards-Version to 4.5.0
    + Add libfreetype6-dev and pkg-config to B-D
    + Set RRR: no
    + Add me as uploader
  * d/copyright: Update year & add myself
  * d/docs: Fix path for installation
  * d/jaaa.manpages: Fix path for installation
  * d/patches: Refresh patches, add pkgconf fix
  * d/rules:
    + Add prefix and pkgconf export
    + Fix man installation
  * d/source/local-options: Drop, obsolete

 -- Dennis Braun <d_braun@kabelmail.de>  Wed, 05 Feb 2020 21:01:12 +0100

jaaa (0.8.4-4) unstable; urgency=medium

  * .gitignore file by postclone script.
  * Add patch to fix cross build (Closes: #870859).
  * Numbered patches.
  * Update copyright file.
  * Patch forwarded.
  * Tune dh version.
  * Bump Standards.
  * Append flags rather to LDFLAGS.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 08 Aug 2017 03:59:31 +0200

jaaa (0.8.4-3) unstable; urgency=medium

  * Set dh 10.
  * Update copyright file.
  * Sign tags.
  * Remove README.debian file.
  * Avoid useless linking.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 22 Dec 2016 13:52:44 +0100

jaaa (0.8.4-2) unstable; urgency=medium

  * Fix description. (Closes: #747639)
  * Bump Standards.
  * Fix VCS fields.
  * Remove menu file.
  * Fix hardening.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 20 Jul 2016 12:08:13 +0200

jaaa (0.8.4-1) unstable; urgency=low

  [ Alessio Treglia ]
  * Unapply patches after the build,abort on upstream changes.
  * Suggests jackd.

  [ Jaromír Mikeš ]
  * Imported Upstream version 0.8.4
  * Add myself as uploader.
  * Set dh/compat 9.
  * Bump Standards.
  * Fix VCS canonical URLs.
  * Tune gitignore file to handle rather dir.
  * Update copyright file.
  * Tune build-deps for new upstream release.
  * Patch refreshed.
  * Tune rules file - Makefile moved by upstream.
  * Fix installing desktop files and icon.
  * Added Keywords entry to desktop files.
  * Make package description more verbose.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 05 Aug 2013 22:31:06 +0200

jaaa (0.6.0-2) unstable; urgency=low

  * Provide two desktop files (LP: #417422):
    - jaaa-alsa.desktop: Launcher to use Jaaa through ALSA.
    - jaaa-jack.desktop: Launcher to use Jaaa through JACK.
  * Improve menu file to achieve the same above.
  * Bump Standards.
  * Update debian/gbp.conf.
  * Rewrite debian/copyright as per DEP-5 rev#173.
  * Upstream's Homepage has changed, update debian/{control,watch} files.

 -- Alessio Treglia <alessio@debian.org>  Thu, 24 Mar 2011 21:05:13 +0100

jaaa (0.6.0-1) unstable; urgency=low

  [ Philippe Coval ]
  * Imported Upstream version 0.6.0
    - Refreshed patch
  * Updated debian/control:
    - Updated standards and format to 3.0 (quilt)
    - Switched to collaborative maintenance
    - Uses alioth scm
    - Removed superfluous build-dependency (Closes: #578088)
  * Updated debian/rules:
    - debian/rules: remove get-orig-source
  * Rewrote manpage to fix manpage-has-errors-from-man

  [ Alessio Treglia ]
  * Fix Maintainer's email address, add myself as uploader.
  * Drop quilt support as it complies to 3.0 (quilt) format.
  * Add debian/gbp.conf file.
  * Add .gitignore file.
  * Switch to DH 7 tiny rules.
  * Refresh patches.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Sat, 26 Jun 2010 21:00:13 +0200

jaaa (0.4.2-2) unstable; urgency=low

  * Merged jaaa_0.4.2-1ubuntu1.patch
  * debian/* : updated to latest standards + maintainer email (Closes: #546630)

 -- Philippe Coval <rzr@gna.org>  Thu, 15 Oct 2009 08:50:41 +0200

jaaa (0.4.2-1ubuntu1) intrepid; urgency=low

  * debian/jaaa.desktop
    - Add AudioVideo to Categories to make it show up in menu (LP: #243773)
    - Add semicolon (;) after Audio to make it validate with desktop-file-
      validate
  * Modify Maintainer value to match the DebianMaintainerField
    specification.
  * Bump standards version to 3.8.0

 -- Nathan Handler <nathan.handler@gmail.com>  Sat, 28 Jun 2008 14:59:39 -0500

jaaa (0.4.2-1) unstable; urgency=low

  * Integration from ubuntu to debian (Closes: #435582)
  * Simplified rules, minors fixes in copright and doc-base files

 -- Philippe Coval <rzr@users.sf.net>  Wed, 23 Jan 2008 19:51:58 +0100

jaaa (0.4.2-0ubuntu1) hardy; urgency=low

  * Initial release (LP: #160732)

 -- Philippe Coval <rzr@users.sf.net>  Thu, 17 May 2007 19:02:48 +0200
